# awdb-nodejs
www.ipplus360.com 官方支持的解析awdb格式的Nodejs代码(Official support for parsing Nodejs code in AWDB format )


1、安装nodejs环境 (node >= 11)
    wget https://cdn.npmmirror.com/binaries/node/v16.17.0/node-v16.17.0-linux-x64.tar.gz
    tar -zxvf node-v16.17.0-linux-x64.tar.gz
    配置node环境变量
    sudo vim /etc/profile
    添加到文件最后
    export NODE_HOME={path}/node-v16.17.0-linux-x64
    export PATH=$PATH:$NODE_HOME/bin
    # 加载环境变量
    source /etc/profile
    node -v查看是否安装成功
2、解析样例
node example.js
